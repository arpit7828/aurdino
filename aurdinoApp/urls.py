from django.conf.urls import url
from . import views

urlpatterns =   [
    # url(r'^home/', views.home, name='home'),
    url(r'^$', views.landing, name='landing'),
    url(r'^getData/$', views.getData, name='getData'),
    url(r'^history$', views.landing_history, name='landing_history'),
    url(r'^getDatahistory/$', views.getDataHistory, name='getDataHistory'),
    # url(r'^searchReg/$', views.searchReg, name='searchReg'),
    
]

