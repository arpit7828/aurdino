
		var table = $('#example').DataTable({	
			dom : 'lfrtip',
		    "bFilter" : false,
		    "scrollX": false,
		    "searching": true,
		    "pageLength": 100,
		    "order": [ [5,'desc'] ],
		    "searchDelay":1000,
		    "processing": true,
		    "bProcessing": false,
		    "serverSide": true,
		    "autoWidth": false,
		    "columnDefs": [
		      { "visible": true,  "targets": [0,] },  
		  ],
		    "aLengthMenu": [
		 [10,25,50,100, 500, 1000],
		 [10,25,50,100, 500, 1000]
		 ],	
			"ajax": {
			 	url : "/get_requests/",
			 	type: "GET",
			 	"data": function ( d ) {
			        d.ref = '2';
			        d.statuss = $("#status").val();
			        // d.veh_type = 'all';
			    }
			},
			"fnPreDrawCallback": function() {
              $('#loader-wrapper').show();
              },
	        "fnDrawCallback": function() {
	              $('#loader-wrapper').hide();
	        },
	      "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull)
	         {
	          }
		 });


		$('body').on('change', '#status', function() {
				// $('#text_need_follow_up').val($(this).val());
				table.ajax.reload();
		});
		



	